# Installing Bunting on Zen Cart

To download the plugin click on Downloads in the left hand menu then click the `Download Repository` link.

To install the `bunting-zencart` plugin, using either using sftp or ssh to connect to your server and locate your Zen Cart
store directory.

Next within copy the `includes` folder into the root directory of your Zen Cart store. Then copy all of the **files** from **within**
the `YOUR_ADMIN` folder to your admin folder.

>Please note that Zen Cart admin folders are randomly generated strings, which are created when your store is setup.
 
After copying the files over, navigate to the Bunting plugin within your stores admin menu under `Tools` ->
`Bunting`.

Sign in to your Bunting account to complete the installation process: Enter your existing bunting account details or
if you are new to Bunting and want to get started, click the `CREATE ACCOUNT` button.