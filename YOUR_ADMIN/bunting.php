<?php

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
require(DIR_WS_CLASSES . 'language.php');
require(DIR_WS_CLASSES . 'bunting.php');

$bunting = new bunting(new currencies(), new language());

if (!empty($_POST)) {
    if (!empty($_POST['verify_email_address'])) {
        return $bunting->buntingVerify();
    } elseif (!empty($_POST['register_email_address'])) {
        return $bunting->buntingRegister();
    }

    zen_redirect(zen_href_link(FILENAME_BUNTING));
} elseif (!empty($_GET)) {
    $bunting->unlinkBunting();

    $messageStack->add_session(BUNTING_TEXT_UNLINK, 'success');

    zen_redirect(zen_href_link(FILENAME_BUNTING));
} elseif ($bunting->isConnected()) {
    $bunting_account_id = zen_get_configuration_key_value('BUNTING_ACOUNT_ID');
    $bunting_timestamp = time();
    $bunting_subdomain = zen_get_configuration_key_value('BUNTING_SUBDOMAIN');
    $bunting_email_address = zen_get_configuration_key_value('BUNTING_EMAIL');
    $bunting_password_api = zen_get_configuration_key_value('BUNTING_PASSWORD_API');
    $account_key = $bunting_subdomain.$bunting_email_address.$bunting_timestamp;
    $bunting_hash = hash_hmac('sha256', $account_key, $bunting->hashKey);
} else {
    $shop_name = STORE_NAME;
    $potential_subdomain = strtolower(preg_replace("/[^A-Za-z0-9]/", '', $shop_name));
    $email_address = STORE_OWNER_EMAIL_ADDRESS;
}

?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<link rel="stylesheet" type="text/css" href="includes/css/bunting.css">
<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script language="javascript" src="includes/javascript/jquery-1.12.1.min.js"></script>
<script language="javascript" src="includes/javascript/validate.js"></script>
<script language="javascript" src="includes/javascript/bunting.js"></script>
<script type="text/javascript">
  <!--
  function init()
  {
    cssjsmenu('navbar');
    if (document.getElementById)
    {
      var kill = document.getElementById('hoverJS');
      kill.disabled = true;
    }
  }
  // -->
</script>
</head>
<body onLoad="init()">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
        <td width="100%" valign="top">
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                <tr>
                    <td>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pageHeading">Bunting</td>
                                <td class="pageHeading" align="right"><?php echo zen_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="bunting-personalisation">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="body-inner">
                                    <?php if ($bunting->isConnected()): ?>

                                        <div class="box-container logged-in">
                                            <h2 class="title">Sign in to <strong>Bunting</strong></h2>
                                            <p>
                                                Account address:<br><strong><?= $bunting_subdomain ?>.bunting.com</strong>
                                            </p>
                                            <form action="https://<?= $bunting_subdomain ?>.bunting.com/login" class="dotted go-to-bunting" target="_blank" method="post">
                                                <input type="hidden" name="a" value="login">
                                                <input type="hidden" name="timestamp" value="<?= $bunting_timestamp ?>">
                                                <input type="hidden" name="hash" value="<?= $bunting_hash ?>">
                                                <input type="hidden" name="password_api" value="<?= $bunting_password_api ?>">
                                                <input type="hidden" name="email_address" value="<?= $bunting_email_address ?>">
                                                <input type="hidden" name="plugin" value="opencart">
                                                <button type="submit" class="btn btn-info">
                                                    Login to your<br>Bunting Account
                                                </button>
                                            </form>
                                            <a href="./bunting.php?unlink=true" onclick="return confirm('Are you sure you want to unlink your Bunting account?');" class="unlink-bunting btn btn-default">
                                                Uninstall
                                            </a>
                                        </div>
                                        <script>
                                            setInterval(function() {
                                                window.location.reload();
                                            }, 58000);
                                        </script>

                                    <?php else: ?>

                                        <form method="post" enctype="multipart/form-data" id="hidden-form" class="form-horizontal">
                                            <input type="hidden" name="module_bunting_status" value="1">
                                        </form>
                                        <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
                                        <div id="loading">
                                            <div class="spinner">
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                        </div>
                                        <div class="box-container" id="loginForm">
                                            <h2 class="title">Add Bunting to <strong>Opencart</strong></h2>
                                            <p style="display: none" class="message"></p>
                                            <div class="dotted">
                                                <h4>New to Bunting?</h4>
                                                <a href="#planChooseForm" class="btn btn-primary btn-lg" id="choosePlanButton">Create Bunting Account</a>
                                            </div>
                                            <div class="row">
                                                <div class="login">
                                                    <h2>Already have a Bunting Account?</h2>
                                                    <form id="actualLoginForm">
                                                        <div class="form-group required">
                                                            <label for="verify_bunting_subdomain">Bunting account subdomain</label>
                                                            <input type="text" class="form-control" id="verify_bunting_subdomain" name="verify_bunting_subdomain" required>
                                                        </div>
                                                        <div class="form-group required">
                                                            <label for="verify_email_address">Email address</label>
                                                            <input type="email" class="form-control" id="verify_email_address" name="verify_email_address" required>
                                                        </div>
                                                        <div class="form-group required">
                                                            <label for="verify_password">Password</label>
                                                            <input type="password" class="form-control" id="verify_password" name="verify_password" required>
                                                        </div>
                                                        <button type="submit" class="btn btn-info">Login</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-container wide" id="registerForm" style="display:none;">
                                            <a class="back" href="#back">Back</a>
                                            <h2 class="title">Create Your <strong>Free Trial Account</strong></h2>
                                            <h4>You're moments away from personalising your website</h4>
                                            <p style="display: none" class="message"></p>
                                            <form id="actualRegisterForm">
                                                <fieldset>
                                                    <h3>Your Bunting Account</h3>
                                                    <div class="form-group required">
                                                        <label for="company_name">Company Name</label>
                                                        <input type="text" class="form-control" id="company_name" name="company_name" value="<?= $shop_name ?>" required>
                                                    </div>
                                                    <div class="form-group required">
                                                        <label for="register_bunting_subdomain">Choose your Bunting account's web address</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon">https://</div>
                                                            <input type="text" class="form-control" id="register_bunting_subdomain" name="register_bunting_subdomain" value="<?= $potential_subdomain ?>" required>
                                                            <div class="input-group-addon">.bunting.com</div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <h3>Your Login Details</h3>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="forename">Forename</label>
                                                                <input type="text" class="form-control" id="forename" name="forename" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="surname">Surname</label>
                                                                <input type="text" class="form-control" id="surname" name="surname" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required">
                                                        <label for="register_email_address">Email Address</label>
                                                        <input type="email" class="form-control" id="register_email_address" name="register_email_address" value="<?= $email_address ?>" required>
                                                        <span class="hint">(Kept safe, never given to others)</span>
                                                    </div>
                                                    <div class="form-group required">
                                                        <label for="telephone_number">Phone</label>
                                                        <input type="text" class="form-control" id="telephone_number" name="telephone_number" value="" required>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="register_password">Password</label>
                                                                <input type="password" class="form-control" id="register_password" name="register_password" required>
                                                                <span class="hint">(We'll encrypt this for security)</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group required">
                                                                <label for="password_confirmation">Confirm password</label>
                                                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <p>
                                                            Do you have a <a id="promoCodeButton" role="button" data-toggle="collapse" href="#promo-code" aria-expanded="false" aria-controls="promo-code">Promo Code</a>?
                                                        </p>
                                                        <div class="collapse" id="promo-code">
                                                            <input type="text" class="form-control" id="promotional_code" name="promotional_code" placeholder="Promo code (optional)">
                                                        </div>
                                                    </div>
                                                    <div class="submit">
                                                        <div class="row">
                                                            <div class="col-sm-6 submit-text">
                                                                <span id="premium-terms">By clicking the button you agree to Bunting's <a href="http://knowledgebase.getbunting.com/bunting-terms-of-service/" target="_blank">Terms of Business</a>.</span>
                                                            </div>
                                                            <div class="col-sm-6 submit-button">
                                                                <button type="submit" class="btn btn-info">Get started</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>

                                    <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
