<?php

if (!defined('IS_ADMIN_FLAG')) {
  die('Illegal Access');
}

class bunting
{
    private $currency;
    private $language;
    private $configGroup;
    public $hashKey = 'dKf-3($ckDFpe3D';

    function __construct($currency, $language)
    {
        $this->currency = $currency;
        $this->language = $language;

        global $db;
        $sql = "SELECT * FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Bunting'";
        $result = $db->execute($sql);
        $this->configGroup = $result->RecordCount() ? $result->fields : null;
    }

    public function isConnected()
    {
        return !is_null($this->configGroup);
    }

    public function buntingVerify()
    {
        $bunting_data = $this->submitToBunting('verify', [
            'email_address' => zen_db_prepare_input($_POST['verify_email_address']),
            'password' => zen_db_prepare_input($_POST['verify_password']),
            'subdomain' => zen_db_prepare_input($_POST['verify_bunting_subdomain'])
        ]);

        return $this->buntingResponse($bunting_data);
    }

    public function buntingRegister()
    {
        global $db;
        $country = $db->Execute("select * from " . TABLE_COUNTRIES . " where countries_id = '" . (int)STORE_COUNTRY . "'");
        $timezone = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country->fields['countries_iso_code_2']);

        $address = $this->processAddress(STORE_NAME_ADDRESS);

        $submit_data = [
            'billing' => 'automatic',
            'email_address' => $_POST['register_email_address'],
            'password' => $_POST['register_password'],
            'confirm_password' => $_POST['password_confirmation'],
            'subdomain' => $_POST['register_bunting_subdomain'],
            'name' => $_POST['company_name'],
            'forename' => $_POST['forename'],
            'surname' => $_POST['surname'],
            'telephone_number' => $_POST['telephone_number'],
            'promotional_code' => $_POST['promotional_code'],
            'timezone' => $timezone[0],
            'country' => $country->fields['countries_iso_code_2'],
            'agency' => 'no'
        ];

        $submit_data = $submit_data+$address;

        $bunting_data = $this->submitToBunting('register', $submit_data);

        $this->buntingResponse($bunting_data);
    }

    private function processAddress($orig_address)
    {
        $comma_address = preg_replace('#[\n]#', ',', trim($orig_address));
        $address_array = explode(',', $comma_address);
        $address = [];
        if (!empty($address_array)) {
            $address['address_line_1'] = array_shift($address_array);
            $address['postcode'] = array_pop($address_array);
            $i = 2;
            foreach ($address_array as $address_line) {
                $address['address_line_'.$i] = $address_line;
                if ($i < 5) {
                    $i++;
                } else {
                    break;
                }
            }
        } else {
            $address['address_line_1'] = $comma_address;
        }
        ksort($address);
        return $address;
    }

    private function submitToBunting($action, $params)
    {
        $languages = [];
        $currencies = [];

        foreach ($this->currency->currencies as $code => $currency) {
            $currencies[] = [
                'currency' => $code,
                'symbol' => (!empty($currency['symbol_left']) ? $currency['symbol_left'] : $currency['symbol_right'])
            ];
        }

        foreach ($this->language->catalog_languages as $code => $language) {
            $languages[] = strtoupper($code);
        }

        $domain = parse_url(HTTP_SERVER);

        $timestamp = time();

        $feed_token = md5(sha1(uniqid(mt_rand(), 1)).$timestamp);

        $default_params = [
            'timestamp' => $timestamp,
            'hash' => hash_hmac('sha256', $timestamp, $this->hashKey),
            'plugin' => 'zencart',
            'domain_name' => $domain['host'],
            'create_website_monitor' => 'yes',
            'website_name' => STORE_NAME,
            'languages' => $languages,
            'currencies' => $currencies,
            'website_platform' => 'Zen Cart',
            'ecommerce' => 'yes',
            'cart_url' => zen_catalog_href_link(FILENAME_SHOPPING_CART),
            'product_feed-url_protocol' => $domain['scheme'] . '://',
            'product_feed-url' => zen_catalog_href_link('bunting', 'token=' . $feed_token)
        ];
        $params = $params+$default_params;
        $params = $this->httpBuildQueryForCurl($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bunting.com/plugins/'.$action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        $data = json_decode($response, true);

        if ($data['success']) {
            $data['email_address'] = $params['email_address'];
            $this->installBunting($data, $feed_token);
        }

        return $data;
    }

    /**
     * Takes a response from the Bunting API and converts it to a valid frontend module response
     *
     * @param array $buntingData
     * @return void
     */
    private function buntingResponse($buntingData)
    {
        $response = [];

        if ($buntingData['success']) {
            $response['message'] = 'You can now login to Bunting.';
            echo json_encode($response);
            die;
        }

        $response['message'] = 'Please review the errors and try again.';

        if (!isset($buntingData['errors']) || !count($buntingData['errors'])) {
            $buntingData['errors'] = [];
            $response['message'] .= '<br><br>There was a problem connecting your shop to Bunting, please contact Bunting support.';
        }

        if (isset($buntingData['errors']['validation'])) {
            $response['message'] .= '<br><br>' . $buntingData['errors']['validation'];
        }

        $response['errors'] = $buntingData['errors'];
        echo json_encode($response);
        die;
    }

    private function httpBuildQueryForCurl($arrays, &$new = array(), $prefix = null)
    {
        if (is_object($arrays)) {
            $arrays = get_object_vars($arrays);
        }

        foreach ($arrays as $key => $value) {
            $k = isset($prefix) ? $prefix.'['.$key.']':$key;
            if (is_array($value) || is_object($value)) {
                $this->httpBuildQueryForCurl($value, $new, $k);
            } else {
                $new[$k] = $value;
            }
        }
        return $new;
    }

    private function installBunting($data, $feed_token)
    {
        global $db;

        $sql = "SELECT * FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Bunting'";
        $group = $db->execute($sql);

        if ($group->RecordCount() < 1) {
            $insert_sql = "INSERT INTO " . TABLE_CONFIGURATION_GROUP . " 
                SET configuration_group_title = 'Bunting',
                    configuration_group_description = 'Bunting Configuration',
                    visible = '0'" ;
            $result = $db->execute($insert_sql);
            $group = $db->execute($sql);
        }
        
        $currentDateTime = new DateTime();
        $currentDateTime = $currentDateTime->format("Y-m-d H:i:s");

        $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_group_id, configuration_description, date_added)
                      values ('" . zen_db_input('Bunting Status') . "',
                              '" . zen_db_input('BUNTING_STATUS') . "',
                              '" . zen_db_input(1) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Acount ID') . "',
                              '" . zen_db_input('BUNTING_ACOUNT_ID') . "',
                              '" . zen_db_input($data['account_id']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Email') . "',
                              '" . zen_db_input('BUNTING_EMAIL') . "',
                              '" . zen_db_input($data['email_address']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Website Monitor ID') . "',
                              '" . zen_db_input('BUNTING_WEBSITE_MONITOR_ID') . "',
                              '" . zen_db_input($data['website_monitor_id']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Unique Code') . "',
                              '" . zen_db_input('BUNTING_UNIQUE_CODE') . "',
                              '" . zen_db_input($data['unique_code']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Subdomain') . "',
                              '" . zen_db_input('BUNTING_SUBDOMAIN') . "',
                              '" . zen_db_input($data['subdomain']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Region Subdomain ID') . "',
                              '" . zen_db_input('BUNTING_REGION_ID') . "',
                              '" . zen_db_input($data['server_region_subdomain_id']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Feed Token') . "',
                              '" . zen_db_input('BUNTING_FEED_TOKEN') . "',
                              '" . zen_db_input($feed_token) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "'),
                              ('" . zen_db_input('Bunting Password API') . "',
                              '" . zen_db_input('BUNTING_PASSWORD_API') . "',
                              '" . zen_db_input($data['password_api']) . "',
                              '" . (int)zen_db_input($group->fields['configuration_group_id']) . "',
                              '',
                              '" . $currentDateTime . "')
                              ");
    }

    public function unlinkBunting()
    {
        global $db;

        $sql = "SELECT * FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Bunting'";
        $group = $db->execute($sql);

        $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_group_id = " . (int)$group->fields['configuration_group_id']);
        $db->Execute("delete from " . TABLE_CONFIGURATION_GROUP . " where configuration_group_id = " . (int)$group->fields['configuration_group_id']);

        return true;
    }

    /**
     * ZenCart's default zen_get_configuration_key_value returns HTML if the key value is empty for some ridiculous reason, I don't know why
     * but I'm not going to put up with it, so I've re-implemented it here and escaped the configuration key
     *
     * @param $lookup
     * @return string|null
     */
    public function zen_get_configuration_key_value($lookup) {
        global $db;
        $sql = "select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = :configuration_key";
        $sql = $db->bindVars($sql, ':configuration_key', $lookup, 'string');
        $lookupValue = $db->Execute($sql)->fields['configuration_value'];

        return $lookupValue ? $lookupValue : null;
    }
}