<?php

if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
}

if (function_exists('zen_register_admin_page')) {
    if (!zen_page_key_exists('toolsBunting')) {
        zen_register_admin_page('toolsBunting', 'BOX_TOOLS_BUNTING', 'FILENAME_BUNTING','' , 'tools', 'Y', 20);
    }
}