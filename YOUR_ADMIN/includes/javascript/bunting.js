jQuery( document ).ready(function( $ ) {
    $('#billing-freemium, #billing-automatic').addClass('btn btn-primary');
    $('#choosePlanButton').click(function(e) {
        e.preventDefault();
        $('#loginForm').fadeOut(function() {
            $('#registerForm').fadeIn();
        });
    });
    $('#registerForm .back').click(function(e) {
        e.preventDefault();
        $('#registerForm').fadeOut(function() {
            $('#loginForm').fadeIn();
        });
    });
    $.validator.addMethod("subdomain", function(value, element) {
        return this.optional( element ) || /^[a-z0-9\-_]+$/.test( value );
    }, "Please specify a valid subdomain, using lowercase letters, numbers, hyphens and underscores.");
    $("#actualLoginForm").validate({
        rules: {
            verify_bunting_subdomain: {
                required: true,
                maxlength: 50
            },
            verify_email_address: {
                required: true,
                email: true
            },
            verify_password: {
                required: true,
                maxlength: 100
            }
        },
        submitHandler: function(form) {
            submitForm(form, 'login', 'verify');
        }
    });
    $("#actualRegisterForm").validate({
        rules: {
            register_email_address: {
                required: true,
                email: true
            },
            register_password: {
                required: true,
                minlength: 8,
                maxlength: 100
            },
            password_confirmation: {
                required: true,
                equalTo: "#registerForm #register_password"
            },
            forename: {
                required: true,
                maxlength: 100
            },
            surname: {
                required: true,
                maxlength: 100
            },
            company_name: {
                required: true,
                maxlength: 100
            },
            register_subdomain: {
                required: true,
                subdomain: true,
                maxlength: 100
            },
            promotional_code: {
                maxlength: 20
            }
        },
        submitHandler: function(form) {
            submitForm(form, 'register', 'register');
        }
    });

    window.allowPasswordTrigger = false;
    jQuery('.forgotPasswordTrigger').click(function(e){
        if (window.allowPasswordTrigger) {
            return true;
        }

        e.preventDefault();
        e.stopPropagation();
        jQuery('.forgotPasswordForm').toggleClass('active');
        return false;
    });

    jQuery('.forgotPasswordForm .btn').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var subdomain = jQuery('.forgotPasswordForm #bunting_forgot_subdomain').val(),
            urlRedirect = 'https://' + subdomain + '.1.bunting.com/login?a=lost_password';

        jQuery.ajax({
            type: "POST",
            url: window.bunting_domain_exists_url,
            data: {subdomain: subdomain},
            dataType: 'json',
            success: function (data) {
                if (parseInt(data) === 1) {
                    window.location = urlRedirect;
                } else {
                    alert('The subdomain you entered is incorrect');
                }
            }
        });
    });
});
function submitForm(form, type, prefix) {
    jQuery('#loading').show();
    var $message = jQuery(form).parents('.box-container').find('p.message'),
        fieldErrorMappings = {
            subdomain: prefix + '_bunting_subdomain',
            email_address: prefix + '_email_address',
            property: prefix + '_password',
            name: 'company_name',
            confirm_password: 'password_confirmation',
            validation: 'verify_password'
        };
    $message.hide();
    jQuery('label.error').hide();
    jQuery('input.error').removeClass('error');

    var $this = jQuery(form),
        values = $this.serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

    jQuery.ajax({
        type: "POST",
        url: window.location.href,
        data: values,
        dataType: 'json',
        success: function(data) {
            if (typeof data.errors === 'undefined') {
                $("form#hidden-form").submit();
            }
            else {
                for (var property in data.errors) {
                    if (data.errors.hasOwnProperty(property)) {
                        var value = data.errors[property];
                        if (property == 'subdomain') {
                            property = prefix+'_bunting_subdomain';
                        }
                        else if (property == 'email_address' || property == 'password') {
                            property = prefix+'_'+property;
                        }
                        else if (property == 'name') {
                            property = 'company_name';
                        }
                        else if (property == 'confirm_password') {
                            property = 'password_confirmation';
                        }
                        else if (property == 'validation') {
                            property = 'verify_password';
                            jQuery('#verify_email_address').addClass('error');
                        }
                        jQuery('#'+property).addClass('error').parent().after('<label class="error">'+value+'</label>');
                    }
                }
            }
            jQuery('#loading').hide();
        },
        always: function() {
            jQuery('#loading').hide();
        }
    });
}

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}