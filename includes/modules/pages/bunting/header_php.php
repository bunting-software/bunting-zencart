<?php

require("administrate/includes/classes/bunting.php");

$bunting = new bunting(new currencies(), new language());

// Check we're connected
if (strpos($bunting->zen_get_configuration_key_value('BUNTING_ACOUNT_ID'), 'span') !== false) {
    exit;
}

// Check token matches
$token = (!empty($_GET['token'])) ? $_GET['token'] : '';
if ($bunting->zen_get_configuration_key_value('BUNTING_FEED_TOKEN') != $token) {
    exit;
}

$subdomain = $bunting->zen_get_configuration_key_value('BUNTING_SUBDOMAIN');
$region = $bunting->zen_get_configuration_key_value('BUNTING_REGION_ID');

$output  = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
$output .= '<!DOCTYPE feed SYSTEM "https://' . $subdomain . '.' . $region . '.bunting.com/feed-' . zen_get_configuration_key_value('BUNTING_WEBSITE_MONITOR_ID') . '.dtd">';
$output .= '<feed>';

$products = bunting_get_products();
$languages = bunting_get_languages();

foreach ($products as $product) {
    $info_page = zen_get_info_page($product['products_id']);
    $tax_rate  = zen_get_tax_rate($product['products_tax_class_id']);

    $output .= '<product>';
    $output .= '  <upc>' . $product['products_id'] . '</upc>';
    $output .= '  <ns>';
    foreach ($languages as $language) {
        $name = zen_get_products_name($product['products_id'], $language['id']);
        $output .= '    <'.$language['code'].'><![CDATA[' . $name . ']]></'.$language['code'].'>';
    }
    $output .= '  </ns>';
    $output .= '  <ps>';
    foreach ($currencies->currencies as $code => $currency) {
        $output .= '    <' . strtolower($code) . '>' . number_format(zen_add_tax($product['products_price']*$currencies->get_value($code), $tax_rate), 2) . '</' . strtolower($code) . '>';
    }
    $output .= '  </ps>';
    $output .= '  <u><![CDATA[' . zen_href_link($info_page, 'products_id=' . $product['products_id'] . $additionalURL, 'NONSSL', false) . ']]></u>';

    if (zen_not_null($product['products_image'])) {
        $products_image = ltrim($product['products_image'], '/');
        require(DIR_WS_MODULES . zen_get_module_directory(FILENAME_MAIN_PRODUCT_IMAGE));
        $output .= '  <iu><![CDATA[' . HTTP_SERVER . DIR_WS_CATALOG . $products_image_large . ']]></iu>';
    }

    $output .= '  <c><![CDATA[' . zen_get_categories_name_from_product($product['products_id']) . ']]></c>';

    if (zen_not_null($product['manufacturers_name'])) {
        $output .= '  <b><![CDATA[' . html_entity_decode($product['manufacturers_name'], ENT_QUOTES, 'UTF-8') . ']]></b>';
    }

    $display_special_price = zen_get_products_special_price($product['products_id'], true);

    if ($display_special_price) {
        $specials = $db->Execute("select * from " . TABLE_SPECIALS . " where products_id = '" . (int)$product['products_id'] . "' and status='1' AND (specials_date_available IS NULL OR specials_date_available = '0001-01-01' OR specials_date_available >= NOW()) AND (expires_date IS NULL OR expires_date = '0001-01-01' OR expires_date >= NOW()) ");

        $output .= '<oss>';
        foreach ($currencies->currencies as $code => $currency) {
            $output .= '    <' . strtolower($code) . '>' . number_format(zen_add_tax($product['products_price'] * $currencies->get_value($code), $tax_rate) - zen_add_tax($specials->fields['specials_new_products_price'] * $currencies->get_value($code), $tax_rate), 2) . '</' . strtolower($code) . '>';
        }
        $output .= '</oss>';

        if ($specials->fields['expires_date'] != '0001-01-01') {
            $to = new DateTime($specials->fields['expires_date']);
            $output .= '  <oe><![CDATA[' . $to->format('U') . ']]></oe>';
        }
    }

    $output .= '  <s>' . ((empty($product['products_quantity'])) ? 'n' : 'y') . '</s>';

    $output .= '</product>';
}

$output .= '</feed>';

function bunting_get_languages()
{
    global $db;

    $languages_array = array();
    $languages_query = "select languages_id, name, code, image, directory
                        from " . TABLE_LANGUAGES . "
                        order by sort_order";

    $languages = $db->Execute($languages_query);

    while (!$languages->EOF) {
        $languages_array[$languages->fields['code']] = array(
            'id' => $languages->fields['languages_id'],
            'name' => $languages->fields['name'],
            'image' => $languages->fields['image'],
            'code' => $languages->fields['code'],
            'directory' => $languages->fields['directory'],
        );
        $languages->MoveNext();
    }

    return $languages_array;
}

function bunting_get_products()
{
    global $db;

    $products_array = array();
    $products_query = "SELECT p.products_type, p.products_id, pd.products_name, p.products_image, p.products_price, p.products_tax_class_id,
        p.products_date_added, m.manufacturers_name, p.products_model, p.products_quantity, p.products_weight, p.product_is_call,
        p.product_is_always_free_shipping, p.products_qty_box_status,
        p.master_categories_id
    FROM " . TABLE_PRODUCTS . " p
    LEFT JOIN " . TABLE_MANUFACTURERS . " m ON (p.manufacturers_id = m.manufacturers_id), " . TABLE_PRODUCTS_DESCRIPTION . " pd
    WHERE p.products_status = 1
    AND p.products_id = pd.products_id
    AND pd.language_id = :languageID";

    $products_query = $db->bindVars($products_query, ':languageID', $_SESSION['languages_id'], 'integer');
    $products = $db->Execute($products_query);

    while (!$products->EOF) {
        $products_array[] = array(
            'products_type' => $products->fields['products_type'],
            'products_id' => $products->fields['products_id'],
            'products_name' => $products->fields['products_name'],
            'products_image' => $products->fields['products_image'],
            'products_price' => $products->fields['products_price'],
            'products_tax_class_id' => $products->fields['products_tax_class_id'],
            'manufacturers_name' => $products->fields['manufacturers_name'],
            'products_quantity' => $products->fields['products_quantity'],
        );
        $products->MoveNext();
    }

    return $products_array;
}